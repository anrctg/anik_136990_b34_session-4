<?php

//add slashes
$myString = "this is string 'with' 'quotation' \  null";
$addSlash = addslashes($myString);
    echo $myString;

echo "<br>";
$str = strrev("anik");

echo $str;

// explode
echo "<hr>";
$names = "anik,saiful,farvez";

$exploded =
explode(",", $names);
echo "<pre>";
var_dump($exploded);
echo "</pre>";
echo "<pre>";
foreach ($exploded as $key => $value){
    echo "index no: ".$key. " value is: ".$value."<br>";
}
echo "</pre>";

//join/implode
echo "<hr>";
$names_array = array("anik","saiful","farvez");
$joda =  join($names,$names_array);
echo "<pre>";
var_dump($joda);
echo "</pre>";

//html entities
echo "<hr>";
$varWithHtmlTags = '<h4>hi i am anik , i jailed at a h4 tag :( </h4>';
$imUsingHtmlEntities = htmlentities($varWithHtmlTags);

var_dump($imUsingHtmlEntities);

// var dump without htmlentities
echo "<hr>";
var_dump($varWithHtmlTags);

//implode
echo "<hr>";
$favColors = array("green","blue","purple");
$useingImplode = implode(" :) ", $favColors);

var_dump($useingImplode);

//trim
echo "<hr>";

$someText = "some text for triming function";

$triming = trim($someText, "somtion");

var_dump($triming);

//money format is not work in windows
//$number = 1234.56;
//setlocale(LC_MONETARY,"en_US");
//echo money_format("The price is %i", $number);
//nl2br will add <br> tag by replacce of \n
echo "<hr>";
$someText2 = "some \ntext \nfor \ntriming \nfunction";

echo nl2br($someText2);
echo "<hr>";
///PHP str_pad() Function will add some text untill the carrecter is being full
$strPadText = "I am anik";
$usingStrPad = str_pad($strPadText, 30, ":)");
echo $usingStrPad;


//str_repeat will repeat a string untill a number
echo "<hr>";
echo str_repeat("anik ", 20);

//str_replace is a function where we can repalce a letter, word, sentence in any string
echo "<hr>";
$str = "Hi, i am anik, i am a freelancer";

$str_re1 = str_replace("anik", "jenny", $str);
$str_re2 = str_replace("freelancer","CSE student at defodil universaity",$str_re1);
echo $str_re2;



